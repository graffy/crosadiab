all: run

a.out: cs_adiabats_2.f90
	gfortran -g -O0 -ffree-line-length-none cs_adiabats_2.f90

.PHONY: run
run: a.out
	./a.out

.PHONY: clean
clean:
	rm -f a.out
	rm -f cross*.dat
	rm -f fort.[3-9][0-9][0-9]