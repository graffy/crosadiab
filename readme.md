cs_adiabats_2 provided by Jérôme Loreau

count adiabats and provide cross sections and rate coefficients for asymetric top-linear molecules

inputs: generated from hibridon's job.eadiab and renamed and adapted
	`fort.<symmetry>00<jtot>` :
		symmetry: up to 4
		- 1 first symmetry
		- 2 second symmetry
		jtot: total angular momentum number
	lvls.dat : energy levels of the systems

		31   2   2   2   0   0   136.563
		<index>   <qnum1>   <qnum2>   <dummy1>  <summy2>    <qnum3>    <energy>

		bastst=TRUE
		# J1 IS KP KO J2 Eint(cm-1) Coeffs
		

outputs:
	cross_<qnum1>_<qnum2>__<qnum3>.dat

